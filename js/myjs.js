

$(document).ready(function(){

	$('[data-toggle="tooltip"]').tooltip();

	 //$("html").niceScroll();
	$(document).scroll(function() {

		if($(this).scrollTop() > 50){
			$('.site-header .first-nav').css('box-shadow','1px 1px 1px rgba(0,0,0,0.5)');
			$('#home-page header.site-header nav.first-nav').css('padding','8px 0px');
		}
		else if($(this).scrollTop() < 50) {
		    $('.site-header .first-nav').css('box-shadow','none');
		    $('#home-page header.site-header nav.first-nav').css('padding','24px 0px');
		}

	});

    $("#sticker").sticky({topSpacing:0});

	$("#owl-demo").owlCarousel({
		    items : 5,
		    itemsDesktop : [1199,4],
		    itemsDesktopSmall:[992,4],
		    itemsTablet:[768,3],
		    itemsTablet:[600,2],
		    itemsMobile:[479,2],
		    itemsMobile:[400,1],
		    lazyLoad : true,
		    pagination:false,
		    navigation : true,
		    transitionStyle : "backSlide",
		    navigationText: [,]
	  });

	 $("#collections-types").owlCarousel({
	 	    autoPlay: true,
	 	    slideSpeed : 500,
            paginationSpeed : 500,
		    items : 5,
		    pagination:true,
		    transitionStyle : "backSlide",
		    navigation : true
	  });

	 $("#owl-type1").owlCarousel({
		    items : 2,
		    itemsDesktop : [1199,2],
		    itemsDesktopSmall:[992,3],
		    itemsTablet:[768,2],
		    itemsMobile:[479,1],
		    navigation : true,
		    pagination:false,
		    navigationText: [,]
	  }); 

	 $("#owl-type2").owlCarousel({
		    items : 2,
		    itemsDesktop : [1199,2],
		    itemsDesktopSmall:[992,3],
		    itemsTablet:[768,2],
		    itemsMobile:[479,1],
		    navigation : true,
		    pagination:false,
		    navigationText: [,]
	  });

	$(".grid_view_sort").click(function(){
       $('.row.show-products.center-block').addClass('Gridview').removeClass('listView');
       //$(this).addClass('sort_active');
       console.log("grid");
       $(".list_view_sort").removeClass('sort_active');
    });
    
    $(".list_view_sort").click(function(){
       $('.row.show-products.center-block').addClass('listView').removeClass('Gridview');
       //$(this).addClass('sort_active');
       console.log("list");
       $(".grid_view_sort").removeClass('sort_active');
    });
    
});